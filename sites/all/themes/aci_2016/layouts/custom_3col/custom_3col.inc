<?php

/**
 * Implements hook_panels_layouts()
 */
function aci_2016_custom_3col_panels_layouts() {
  $items['custom_3col'] = array(
    'title' => t('3 columns ZF'),
    'category' => t('CUSTOM 2016'),
    'icon' => 'custom_3col.png',
    'theme' => 'custom_3col',
    //'admin css' => '../foundation_panels_admin.css',
    'regions' => array(
      'left' => t('Left'),
      'middle' => t('Middle'),
      'right' => t('Right'),
      'bottom' => t('bottom'),
    ),
  );
  return $items;
}

