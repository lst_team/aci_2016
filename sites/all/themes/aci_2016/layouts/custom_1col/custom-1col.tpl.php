<?php
/**
 * @file
 * Template for a 3 column panel layout.
 *
 * This template provides a very simple "one column" panel display layout.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   $content['middle']: The only panel in the layout.
 */
?>

<div class="off-canvas-wrap" data-offcanvas>
  <div class="inner-wrap">

    <aside class="left-off-canvas-menu">
      <?php print $content['off_left']; ?>
    </aside>

    <div class="row collapse"><?php print $content['left']; ?></div>

    <a class="exit-off-canvas"></a>

  </div>
</div>

