<div role="document" class="page">

  <?php if (!empty($page['header'])): ?>
    <header role="banner" class="row">
      <div class="columns">
        <?php print render($page['header']); ?>
      </div>
    </header>
  <?php endif; ?>


  <?php if ($messages && !$zurb_foundation_messages_modal): ?>
    <section class="row messages">
      <div class="columns">
        <?php if ($messages): print $messages; endif; ?>
      </div>
    </section>
  <?php endif; ?>

  <main role="main" class="row collapse">
    <div class="<?php print $main_grid; ?> columns">
      <a id="main-content"></a>
      <?php if ($title): ?>
        <h1><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($page['content']); ?>
    </div>
  </main>


  <?php if (!empty($page['footer'])): ?>
    <footer class="row" role="contentinfo">
      <?php print render($page['footer']); ?>

      <?php if ($site_name) : ?>
        <div class="copyright columns">
          &copy; <?php print date('Y') . ' ' . $site_name . ' ' . t('All rights reserved.'); ?>
        </div>
      <?php endif; ?>
    </footer>
  <?php endif; ?>

  <?php if ($messages && $zurb_foundation_messages_modal): print $messages; endif; ?>
</div>